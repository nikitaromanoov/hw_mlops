from setuptools import setup, find_packages

setup(
    name="hw_mlops",
    version="0.1.0",
    packages=find_packages(),
    description="Requirements for hw_mlops",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Romanov Nikita",
    author_email="nikitaromanoov@gmail.com",
    url="https://example.com/project-home-page",
    install_requires=[
        "notebook",
        "kaggle",
        "matplotlib ",
        "seaborn",
        "plotly",
        "numpy",
        "pandas",
    ],
    classifiers=[
        "Programming Language :: Python :: 3.9",
    ],
)
